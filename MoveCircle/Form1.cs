﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MoveCircle
{
    public partial class Form1 : Form
    {
		//
		// Constants
		//
		private const int DefaultWidth  = 700;
		private const int DefaultHeight = 700;
		private const int DefaultRadius = 50;
		private const int DefaultSpeed  = 50;

		//
		// Fields
		//
		private WindowForPoints windowforpoints = null;

		//
		// Constructors
		//
        public Form1()
        {
            InitializeComponent();

            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.StartPosition = FormStartPosition.CenterParent;

			SetDefaultValues();
        }

		//
		// Private Methods
		//
        private void btSet_Click(object sender, EventArgs e)
        {
            try
            {
                int x = Convert.ToInt32(tbSizeA.Text);
				int y = Convert.ToInt32(tbSizeB.Text);
				int r = Convert.ToInt32(tbCircleR.Text);
				int v = Convert.ToInt32(tbV.Text);

                windowforpoints = new WindowForPoints(r,v);
                windowforpoints.Size = new Size(x, y);
                windowforpoints.FormClosed += Windowforpoints_FormClosed;

                this.Hide();

                windowforpoints.Show();
                
            }
            catch(FormatException er)
            {
                MessageBox.Show(er.Message);
                tbCircleR.Clear();
                tbSizeA.Clear();
                tbSizeB.Clear();
            }
        }

        private void Windowforpoints_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Show();
        }

		private void SetDefaultValues()
		{
			tbSizeA.Text   = DefaultWidth.ToString();
			tbSizeB.Text   = DefaultHeight.ToString();
			tbV.Text 	   = DefaultSpeed.ToString();
			tbCircleR.Text = DefaultRadius.ToString();
		}
    }
}
