﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoveCircle
{
	class LagrangeInterpolation
	{
		//
		// Properties
		//
		public List<Point> Points { get; set; }
		public decimal Step { get; set; }


		//
		// Constructors
		//
		public LagrangeInterpolation(List<Point> points, decimal step)
		{
			Points = points;
			Step = step;
		}

		//
		// Public methods
		//
		public List<Point> CalculatePoints()
		{
			List<Point> res = new List<Point>();
			int i = 0;
			decimal currentPoint = Points[0].X + Step * i;

			while (currentPoint <= Points[Points.Count-1].X)
			{
				res.Add(new Point((int)currentPoint, (int)CalculatePoint(currentPoint)));
				i++;
				currentPoint = Points[0].X + Step * i;
			}

			return res;
		}

		//
		// Private methods
		//
		private decimal CalculateL(decimal x, int number)
		{
			decimal res = 1M;

			for(int i = 0; i < Points.Count; i++)
			{
				if (i != number) 
				{
					decimal denominator = x - (decimal)Points[i].X;
					decimal numerator = (decimal)(Points[number].X - Points[i].X);

					//if (numerator == 0) {
					//	numerator = 0.001M;
					//}

					res *= (denominator / numerator);
				}
			}

			return res;
		}

		private decimal CalculatePoint(decimal x)
		{
			decimal res = 0.0M;

			for(int i = 0; i < Points.Count; i++)
			{
				decimal coefficient = CalculateL(x, i);
				res += (coefficient * (decimal)Points[i].Y);
			}

			return res;
		}

	}
}
