﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoveCircle.Helper
{
    public static class LineCutter
    {
        public static List<List<Point>> ChangeLine(List<Point> points)
        {
            List<List<Point>> linePoints = new List<List<Point>>();
            List<Point> oneLine = new List<Point>();

			Direction direction = Direction.Direct;

			for (int i = 0; i < points.Count - 1; i++)
            {
				oneLine.Add(points[i]);

				if (points[i + 1].X < points[i].X && direction == Direction.Direct)
                {
					HandleNoRotateCase (oneLine, points, linePoints, i);
					direction = Direction.Reverse;
                }

				if (points[i + 1].X > points[i].X && direction == Direction.Reverse)
                {
                    RotateLine(oneLine);
					HandleNoRotateCase (oneLine, points, linePoints, i);
                    direction = Direction.Direct;
                }

				if(i == points.Count - 2)
                {
					HandleRotateCase (oneLine, points, linePoints, i, direction);
                }
            }

			return linePoints;
        }

		private static void HandleNoRotateCase(List<Point> oneLine, List<Point> points, 
			List<List<Point>> linePoints, int index)
		{
			linePoints.Add(new List<Point>(oneLine));
			oneLine.Clear();
			oneLine.Add(points[index]);
		}

		private static void HandleRotateCase(List<Point> oneLine, List<Point> points, 
			List<List<Point>> linePoints, int index, Direction currentDirection)
		{
			oneLine.Add(points[index+1]);

			if (currentDirection == Direction.Reverse)
			{
				RotateLine(oneLine);
			}

			linePoints.Add(oneLine);
		}

        public static void RotateLine(List<Point> point)
        {
            List<Point> res = new List<Point>();
            int n = point.Count - 1;

            for(int i = 0; i < point.Count/2; i++)
            {
                Point t = point[i];
                point[i] = point[n - i];
                point[n - i] = t;
            }
        } 
    }
}
