﻿using MoveCircle.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MoveCircle
{
    public partial class WindowForPoints : Form
    {
        //
        // Constants
        //
        private const string ManualMessage = "Click on window to add points and click 'Start'";

        //
        // Properties
        //
        public List<Point> InputPoints { get; set; } = new List<Point>();
        public List<Point> RoutePoints { get; set; } = new List<Point>();
        public int CircleSpeed { get; set; } = 0;
        public int CircleRadius { get; set; } = 0;
        public int CurrentPointIndex { get; set; } = 0;

        // Private Fields
        private System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
        private Graphics g;

        //
        // Constructors
        //
        public WindowForPoints(int radius, int speed)
        {
            InitializeComponent();
            DoubleBuffered = true;

            CircleRadius = radius;
            CircleSpeed = speed;
            g = this.CreateGraphics();

            timer.Tick += Timer_Tick;
            button1.Enabled = false;
        }

        // Public methods

        public void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        // Protected methods

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            timer.Stop();
        }

        protected override void OnShown(EventArgs e)
        {
            base.OnShown(e);
            MessageBox.Show(ManualMessage);
        }

        //
        // Private methods
        //
        private void Timer_Tick(object sender, EventArgs e)
        {
            g.Clear(Color.White);

            g.FillEllipse(
                Brushes.Red,                                            // ellipse color
                RoutePoints[CurrentPointIndex].X - CircleRadius / 2,        // ellipse X
                RoutePoints[CurrentPointIndex].Y - CircleRadius / 2,    // ellipse Y
                CircleRadius,                                           // ellipse width
                CircleRadius                                            // ellipse height
            );

            CurrentPointIndex++;

            if (CurrentPointIndex == RoutePoints.Count)
            {
                timer.Stop();
            }

            Draw();
        }

        private void WindowForPoints_MouseClick(object sender, MouseEventArgs e)
        {
            InputPoints.Add(new Point(e.X, e.Y));
            g = this.CreateGraphics();
            g.FillEllipse(Brushes.Black, e.X - 2, e.Y - 2, 5, 5);

            if (InputPoints.Count > 3)
            {
                button1.Enabled = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            List<List<Point>> lines = LineCutter.ChangeLine(InputPoints);

            for (int j = 0; j < lines.Count; j++)
            {
                LagrangeInterpolation lagrange = new LagrangeInterpolation(lines[j], 1);
                List<Point> resultpoint = lagrange.CalculatePoints();

                if (j % 2 == 0)
                {
                    RoutePoints.AddRange(resultpoint);
                }
                else
                {
                    LineCutter.RotateLine(resultpoint);
                    RoutePoints.AddRange(resultpoint);
                }
            }

            timer.Interval = 1000 / CircleSpeed;
            timer.Start();
        }

        private void Draw()
        {
            Graphics g2 = this.CreateGraphics();

            for (int i = 0; i < InputPoints.Count; i++)
            {
                g2.FillEllipse(Brushes.Red, InputPoints[i].X, InputPoints[i].Y, 5, 5);
            }

            for (int i = 0; i < RoutePoints.Count; i++)
            {
                g2.FillEllipse(Brushes.Blue, RoutePoints[i].X, RoutePoints[i].Y, 2, 2);
            }
        }
    }
}
